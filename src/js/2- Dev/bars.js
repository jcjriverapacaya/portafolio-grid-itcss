// animacion a los skills

const bar1 = document.getElementById("bar1"),
      bar2 = document.getElementById("bar2"),
      bar3 = document.getElementById("bar3"),
      bar4 = document.getElementById("bar4"),
      bar5 = document.getElementById("bar5"),
      bar6 = document.getElementById("bar6"),
      bar7 = document.getElementById("bar7"),
      bar8 = document.getElementById("bar8")

function animateBar(e, mw) {
        e.style.animation = "animateBar 2s linear forwards";
        e.style.maxWidth = mw;
}

if (  window.location.pathname === '/about.html'){
    addEventListener("load",()=> animateBar(bar1, bar1.textContent))
    addEventListener("load",()=> animateBar(bar2, bar2.textContent))
    addEventListener("load",()=> animateBar(bar3, bar3.textContent))
    addEventListener("load",()=> animateBar(bar4, bar4.textContent))
    addEventListener("load",()=> animateBar(bar5, bar5.textContent))
    addEventListener("load",()=> animateBar(bar6, bar6.textContent))
    addEventListener("load",()=> animateBar(bar7, bar7.textContent))
    addEventListener("load",()=> animateBar(bar8, bar8.textContent))
}

