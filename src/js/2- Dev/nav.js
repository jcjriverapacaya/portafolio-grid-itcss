const HeaderToggler = document.getElementById('Header-toggler'),
  BtnClose = document.getElementById('Nav-close'),
  Nav = document.getElementById('Nav'),
  LayoutContainer = document.getElementById('LayoutContainer'),
  Content = document.getElementById('Content')


const ActiveMenu = () => {
  Nav.classList.toggle('is-show')
  LayoutContainer.classList.toggle('is-hidden')
}
const CloseMenu = () => {
  Nav.classList.remove('is-show')
  LayoutContainer.classList.remove('is-hidden')
}

HeaderToggler.addEventListener('click', ActiveMenu)
BtnClose.addEventListener('click', CloseMenu)
Content.addEventListener('click', CloseMenu)