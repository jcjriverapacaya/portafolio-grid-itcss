
  // Initialize and add the map
function initMap() {
  // The location
  let coords = {lat: -11.886862, lng: -77.018860};
  // The map
  let map = new google.maps.Map(
      document.getElementById('Map'), {zoom: 16, center: coords});
  // The marker
  let marker = new google.maps.Marker({position: coords, map: map});
}

